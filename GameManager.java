public class GameManager{
	private Deck pileOfCards;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.pileOfCards = new Deck();
		pileOfCards.shuffle();
		this.centerCard = pileOfCards.drawTopCard();
		this.playerCard = pileOfCards.drawTopCard();
	}
	
	public String toString(){
		return "-----------------------" + "\n" + "Center Card: " + this.centerCard 
		+ "\n Player Card: " + this.playerCard + "\n -------------------------------";
	}
	
	public void dealCards(){
		pileOfCards.shuffle();
		this.centerCard = pileOfCards.drawTopCard();
		
		pileOfCards.shuffle();
		this.playerCard = pileOfCards.drawTopCard();
	}
	
	public int getNumberOfCards(){
		return pileOfCards.length();
	}
	
	public int calculatePoints(){
		if(this.centerCard.getValue() == this.playerCard.getValue())
			return 4;
		if(this.centerCard.getSuit() == this.playerCard.getSuit())
			return 2;
		else
			return -1;
	}
}